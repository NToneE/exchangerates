import asyncio
from pyrogram import types
import app
from app import client, display_names
from app.api import exchange, crypto
import config


_REGIONAL_INDICATOR_A = ord('\N{REGIONAL INDICATOR SYMBOL LETTER A}')


def _get_flag(currency_code: str) -> str:
    first = ord(currency_code[0]) - ord('A')
    second = ord(currency_code[1]) - ord('A')
    return chr(_REGIONAL_INDICATOR_A + first) + chr(_REGIONAL_INDICATOR_A + second)

def _format_float(value: float, precision: int):
    return f'{value:,.{precision}f}'.replace(',', '\'')

def _format_currency(currency_code: str, value: float):
    if currency_code in app.cryptocurrency_codes:
        # avoid values like 0.6000000000 XMR
        fmt_value = _format_float(value, app.CRYPTO_PRECISION).rstrip('0')
        if fmt_value.endswith('.'):
            fmt_value = fmt_value[:-1]
        return f'🐳 {fmt_value} {display_names[currency_code]}'
    else:
        return f'{_get_flag(currency_code)} {_format_float(value, app.CURRENCY_PRECISION)} {display_names[currency_code]}'


@client.on_message()
async def on_message(_client, message: types.Message):
    # let's find something like:
    # '1$' '1 $' '1 доллар'
    # case 1, 2 => regexp
    # case 3 => find all entries of currencies and ensure there is a number before it
    if message.text == '/start':
        await message.reply(getattr(config, 'BOT_MOTD', f'{_get_flag("RUR")} Привет!'))
        return
    base_text = message.text or message.caption
    if base_text is None:
        return
    text_split = base_text.split()
    base_text = ' '.join(text_split).lower()
    queries: set[tuple[float, str]] = set()
    regex_entries = app.symbols_regexp.findall(base_text)
    for match in regex_entries:
        queries.add((float(match[0]), app.currency_symbols[match[1]]))
    split_i = 0
    current_split_data = None
    for i in range(len(base_text)):
        if current_split_data is not None:
            cur = app.currency_names_trie
            cur_i = i
            while cur_i < len(base_text) and isinstance(cur, dict):
                cur = cur.get(base_text[cur_i])
                cur_i += 1
            if isinstance(cur, str):
                queries.add((current_split_data, cur))
        if base_text[i] == ' ':
            try:
                current_split_data = float(text_split[split_i])
            except ValueError:
                current_split_data = None
            split_i += 1
        else:
            current_split_data = None
    if queries:
        res_message = []
        tasks = await asyncio.gather(exchange.api(), crypto.binance_api(), crypto.ton_api())
        currency_rates = tasks[0] | tasks[1] | tasks[2]
        for query_amount, query_currency in queries:
            cur_message = _format_currency(query_currency, query_amount) + ' =\n'
            currencies = []
            for base_currency in app.BASE_CURRENCIES:
                if base_currency != query_currency:
                    val = query_amount * currency_rates[base_currency] / currency_rates[query_currency]
                    currencies.append(_format_currency(base_currency, val))
            cur_message += '> ' + '\n> '.join(currencies)
            res_message.append(cur_message)
        await message.reply('\n\n'.join(res_message))
