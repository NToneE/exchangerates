import json
import os

# file based on https://acdn.tinkoff.ru/mp-resources/currencies.json
CURRENCY_FILENAME = os.path.join('resources', 'currencies.json')
CRYPTO_FILENAME = os.path.join('resources', 'crypto.json')


def get_currencies(file) -> tuple[dict[str, str], dict[str, str], dict[str, str]]:
    result_names = {}
    result_symbols = {}
    result_display = {}
    with open(file, 'r') as f:
        data = json.load(f)
    for currency in data:
        currency_name = currency['currency']['name']
        if 'display_name' in currency:
            result_display[currency_name] = currency['display_name']
        else:
            result_display[currency_name] = currency_name
        result_names[currency_name] = currency_name
        for spelling_field in currency['spelling']:
            result_names[spelling_field] = currency_name
        result_symbols[currency['symbol']] = currency_name
    return result_names, result_symbols, result_display
