from app import cryptocurrency_codes
from app.api import BaseAPI

_BASE = 'USDT'


class BinanceAPI(BaseAPI):
    def process_json(self, json) -> dict[str, float]:
        result = {}
        for entry in json:
            if entry["symbol"].endswith(_BASE):
                cryptocurrency = entry["symbol"][:-(len(_BASE))]
                if cryptocurrency in cryptocurrency_codes:
                    result[cryptocurrency] = 1 / float(entry["price"])
        return result


class KucoinAPI(BaseAPI):
    def process_json(self, json) -> dict[str, float]:
        return {self._url_format_args[0]: 1 / float(json["data"]["price"])}


binance_api = BinanceAPI('https://api.binance.com/api/v3/ticker/price', 10)
ton_api = KucoinAPI('https://api.kucoin.com/api/v1/market/orderbook/level1?symbol={}-USDT', 10, ['TON'])
