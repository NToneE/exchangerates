import abc
import asyncio
import time
import typing

import aiohttp
from aiohttp import ClientSession


class BaseAPI(metaclass=abc.ABCMeta):
    _cooldown: int
    _last_result: typing.Optional[dict[str, float]]
    _last_result_time: float
    _lock: asyncio.Lock
    _session: typing.Optional[aiohttp.ClientSession]
    _url: str
    _url_format_args: list[str]

    def __init__(self, url: str, cooldown: int, url_format_args: typing.Optional[list[str]] = None):
        self._cooldown = cooldown
        self._lock = asyncio.Lock()
        self._last_result = None
        self._last_result_time = 0
        self._session = None
        self._url = url
        self._url_format_args = url_format_args if url_format_args is not None else []

    async def _async_init(self):
        self._session = ClientSession()

    @abc.abstractmethod
    def process_json(self, json) -> dict[str, float]:
        ...

    async def __call__(self) -> dict[str, float]:
        async with self._lock:
            if self._last_result_time + self._cooldown > time.time():
                return self._last_result
            self._last_result_time = time.time()
            if self._session is None:
                await self._async_init()
            async with self._session.get(self._url.format(*self._url_format_args)) as got:
                json = await got.json()
                self._last_result = self.process_json(json)
            return self._last_result
