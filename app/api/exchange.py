from app import openexchangerates_org_app_id
from app.api import BaseAPI


class OpenExchangeRatesAPI(BaseAPI):
    def process_json(self, json) -> dict[str, float]:
        return json['rates']


api = OpenExchangeRatesAPI('https://openexchangerates.org/api/latest.json?app_id={}', 10 * 60,
                           [openexchangerates_org_app_id])
