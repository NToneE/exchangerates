import time
import os
import re
import pyrogram
import uvloop
import app.parse_resources

BASE_CURRENCIES = ['RUB', 'USD', 'EUR',
                   'TON', 'XMR', 'NEAR']

CURRENCY_PRECISION = 2
CRYPTO_PRECISION = 10


def build_trie(currency_names: dict[str, str]) -> dict:
    res = {}
    for origin_name in currency_names:
        name = origin_name.lower()
        cur = res
        next_symbol = 0
        while next_symbol < len(name) - 1 and name[next_symbol] in cur:
            cur = cur[name[next_symbol]]
            next_symbol += 1
        while next_symbol < len(name) - 1 and not isinstance(cur, str):
            cur[name[next_symbol]] = {}
            cur = cur[name[next_symbol]]
            next_symbol += 1
        if not isinstance(cur, str):
            cur[name[next_symbol]] = currency_names[origin_name]
    return res


uvloop.install()

try:
    import config
    pyrogram_credentials = {
        'api_id': config.API_ID,
        'api_hash': config.API_HASH,
        'bot_token': config.TELEGRAM_TOKEN
    }
    openexchangerates_org_app_id = config.OPENEXCHANGERATES_ORG_APP_ID
    del config
except (ModuleNotFoundError, AttributeError) as e:
    raise RuntimeError("Add config.py file in app working directory with API_ID, API_HASH, TELEGRAM_TOKEN and "
                       "OPENEXCHANGERATES_ORG_APP_ID variables")

os.makedirs('sessions', exist_ok=True)
session_file = os.path.join(os.getcwd(), 'sessions', pyrogram_credentials['bot_token'].split(':')[0])

client = pyrogram.Client(session_file, **pyrogram_credentials)

currency_names, currency_symbols, currency_display = app.parse_resources.get_currencies(app.parse_resources.CURRENCY_FILENAME)
cryptocurrency_names, cryptocurrency_symbols, cryptocurrency_display = app.parse_resources.get_currencies(app.parse_resources.CRYPTO_FILENAME)
cryptocurrency_codes = set(cryptocurrency_symbols.values())

display_names = currency_display | cryptocurrency_display

_symbols_regexp_base = f"({'|'.join(map(re.escape, currency_symbols | cryptocurrency_symbols))})"
symbols_regexp = re.compile(fr"([\d.]+)\s?{_symbols_regexp_base}")

currency_names_trie = build_trie(currency_names | cryptocurrency_names)
